dest <- "/global/project/projectdirs/genepool/www/jgi-cori-quota/"
repo_mon_csv <- paste0( dest, 'repo-mon.csv' )
data <- read.csv( header=TRUE, file=repo_mon_csv )
scale <- 1000000
data$Used <- data$Used / scale
data$Allocated <- data$Allocated / scale

plotRepo <- function( rIn=data, repo, window=7 ) {
  if ( ! interactive() ) {
    png( filename= paste0( dest, repo, ".png" ),
         width=512,
         height=512 
       )
  }
  r <- rIn[ rIn$Repo == repo, ]
  x <- min( r$YDay ) + 1
  y <- max( r$Used ) - ( max( r$Used ) - min( r$Used ) ) * 0.15
  plot( Used ~ YDay,
    data=r,
    pch=19,
    col='blue',
    main='JGI partition usage',
    xlab='Day of year',
    ylab='Millions of hours'
  )

  r <- tail( r, window )
  l <- lm( Used ~ YDay, data=r )
  rate <- 0
  rShow <- 0
  units <- "hours"
  if ( is.finite( l$coeff[ 1 ] )  && is.finite( l$coeff[ 2 ] ) )
  {
    abline( l )

    rate <- l$coeff[ 2 ]
    rShow <- rate
    units <- 'Mhours'
#   Sanity check!
    if ( scale != 1000000 && units == 'Mhours' ) {
      stop( "You've changed the scale, but not the units!" )
    }

    if ( rShow < 1 ) {
      rShow <- rShow * 1000
      units = 'Khours'
    }
    if ( rShow < 1 ) {
      rShow <- rShow * 1000
      units = 'hours'
    }
  }

  text <- paste0( "Repository: ", repo, "\n" )
  text <- paste0( text, "Average rate: ", round( rShow, 2 ), " ", units, " per day\n" )
  n <- nrow( r )
  quotaLeft <- r$Allocated[ n ] - r$Used[ n ]
  if ( rate < 0.01 ) {
    daysLeft <- 10000
  } else {
    daysLeft <- round( quotaLeft / rate, 1 )
  }
  text <- paste0( text, "Current allocation: ", round( r$Allocated[ n ], 3 ), " Mhours\n" )
  text <- paste0( text, "Remaining allocation: ", round( quotaLeft, 3 ), " Mhours\n" )
  text <- paste0( text, "Days till exhaustion: ", daysLeft, "\n" )
  text( x=x, y=y, labels=text, pos=4 )
  if ( daysLeft < 30 ) {
    cat( paste0( text, "\n" ) )
  }

  fileConn<-file( paste0( dest, repo, ".sh" ) )
  writeLines( c(
    paste0( "REPO_HOURS_ALLOCATED=", sprintf( "%.0f", r$Allocated[ n ] * scale ) ),
    paste0( "REPO_HOURS_REMAINING=", sprintf( "%.2f", quotaLeft * scale ) ),
    paste0( "REPO_HOURS_TILL_EXHAUSTED=", daysLeft * 24 )
  ), fileConn )
  close( fileConn )
}

repos <- sort( levels( data$Repo ) )
if ( !interactive() ) {
  for ( repo in repos ) {
    r <- data[ data$Repo == repo, ]
    nAlloc <- length( r$Allocated )
    if ( nAlloc < 2 ) {
      next
    }
    lastAlloc <- r$Allocated[ nAlloc ]
    if ( lastAlloc > 0 ) {
      plotRepo( rIn=r, repo=repo )
    }
  }
}
