#!/usr/bin/env perl
use strict;
use warnings;
use JSON::PP;

my ($root,$url,$repo,@repos,$json,$h,$allocated,$used,$year,$month,$day,$yday,$csv,$dest);
$root  = "/global/homes/w/wildish/Work/JGI/repo-mon";
$url   = "http://nimprod.nersc.gov:8004/getnim2/host/cori/repo";
$dest  = "/global/project/projectdirs/genepool/www/jgi-cori-quota/";
$csv   = "$dest/repo-mon.csv";
@repos = qw / fungalp fnglanot plant gentechp synbio microbio /;

chdir $root or die "cd $root: $!\n";

if ( ! -f $csv ) {
  open CSV, ">$csv" or die "open $csv: $!\n";
  print CSV "Year,Month,Day,YDay,Repo,Allocated,Used\n";
  close CSV
}

open CSV, ">>$csv" or die "append $csv: $!\n";

foreach $repo ( @repos ) {
  open WGET, "wget --quiet -O - $url/$repo/json |" or die "wget $url/$repo/json: $!\n";
  $json = "";
  while ( $_ = <WGET> ) {
    $json .= $_;
  }
  $h = decode_json( $json );
  $allocated = $h->{ items }[ 0 ]{ allocated };
  $used      = $h->{ items }[ 0 ]{ amount_charge };
  @_ = localtime();
  ( $day, $month, $year, $yday ) = @_[ 3, 4, 5, 7 ];
  $month += 1;
  $year += 1900;
  print CSV "$year,$month,$day,$yday,$repo,$allocated,$used\n";
}

close CSV or die "close $csv: $!\n";

system( "umask 022; /global/common/cori/software/R/3.3.1/bin/Rscript ./repo-mon.R" );
